## Objectives

1. Students will gain an understanding of JavaScript fundamentals including:

  * Data Types
  * Variables
  * Syntax and Operators
  * Conditionals
  * Functions
  * Debugging and Logging
  * Scope
  * Arrays
  * Objects
  * Loops and Iteration
  * Object Oriented JS
  * Closures
 
## Resources

* [Intro to JavaScript](https://www.w3schools.com/js/js_intro.asp)
* [MDN's JavaScript Intro](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
* [JS Reference Materials](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [A Reference Guide for Modern JS - All the New Stuff](https://github.com/mbeaudru/modern-js-cheatsheet)
* [ECMAScript 6](http://es6-features.org/)
* [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20&%20closures/README.md#you-dont-know-js-scope--closures)

### Loops and Iteration

* [Loops - MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)

### Object Oriented JavaScript

* [OO JavaScript - MDN](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS)
* [Object Oriented JavaScript](http://www.learn-js.org/en/Object_Oriented_JavaScript)

### Closures

* [Closures - MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
* [Closures - YDKJS](https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20%26%20closures/ch5.md)
